FROM registry.gitlab.com/lightweightmanufacturing/odoo/odoo

COPY ./openerp-server.conf /etc/odoo/openerp-server.conf
