# docker-services

This repo contains the dockerfiles and scripts necessary to deploy LWM's various services

It uses different passwords from those in the repo it's pulling from to allow us to keep our server secure while abiding by the copyright restrictions.

While the ERP source code we create must be shared with all users, there is no obligation to make the files in this repo open to anyone

<b>Never share these passwords with anyone</b>, ever. They allow direct access to the database, including copying, editing, and deleting contents.
